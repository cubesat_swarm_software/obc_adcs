/*
 * firmware_updates.h
 *
 *  Created on: 4 апр. 2021 г.
 *      Author: Ilia
 */

#ifndef COMMANDS_FWUPDATE_H_
#define COMMANDS_FWUPDATE_H_



#include "stm32l4xx_hal.h"

#define BUFFER_CRC_SIZE_WORDS         (FLASH_APP_SIZE - 4)/4

typedef enum
{
  FW_IDLE            =  0x00,
  FW_WRITE_READY     =  0x01,
  FW_UPDATE_DONE     =  0x02,
  FW_UPDATE_ERROR    =  0x03,
} fwupdate_StatusTypeDef;


#pragma push(pack)
#pragma pack(1)
struct fwupdate_state
{
	uint8_t  state;
	uint32_t address;
    uint32_t fw_address_current;
    uint16_t fw_size_current;
    uint16_t fw_package_current;
} ;
#pragma pop(pack)

uint16_t fwupdate_erase(uint8_t sender, uint8_t* data);
uint16_t fwupdate_program(uint8_t sender, uint8_t* data);
uint32_t fwupdate_set_bootloader_flag();
struct fwupdate_state * fwupdate_get();

#endif /* COMMANDS_FWUPDATE_H_ */
