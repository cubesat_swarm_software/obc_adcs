/*
 * cmd_OBC.c
 *
 *  Created on: 9 апр. 2021 г.
 *      Author: Ilia
 */

#include "cmd_OBC.h"
#include "log_utils.h"
#include "settings.h"
#include "globalstar_Danil.h"
#include "usart.h"
static const char *tag = "cmd_OBC";

int globalstar_send(uint8_t sender, uint8_t *data){
	uint8_t data_gps[20];
	for(uint8_t i=0;i<20;i++){
		data_gps[i] = i;
	}
	int res = GS_Send_Data(&huart2, 20, data_gps);
	return res;
}
