/*
 * status.h
 *
 *  Created on: 6 апр. 2021 г.
 *      Author: Ilia
 */

#ifndef SETTINGS_STATUS_H_
#define SETTINGS_STATUS_H_
#include "board.h"

#pragma push(pack)
#pragma pack(1)

struct status {
	uint8_t state;
    uint8_t gps_en:1;
    uint8_t globalstar_en:1;
    uint8_t coil_en:1;
    uint8_t bb_en:1;
    uint8_t tel_UHF_en:1;
    uint8_t tel_sd_log:1;
    uint8_t sens_err[SENS_OBC_COUNT];
};
#pragma pop(pack)
typedef enum {IDLE} status_state;

struct status* status_get();
void 		   status_init(void);
#endif /* SETTINGS_STATUS_H_ */
