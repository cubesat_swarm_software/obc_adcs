/*
 * globalstar.h
 *
 *  Created on: 1 июн. 2021 г.
 *      Author: Ilia
 */

#ifndef DRIVERS_GLOBALSTAR_H_
#define DRIVERS_GLOBALSTAR_H_
#include "main.h"
#include "stdbool.h"
void globalstar_module_init(UART_HandleTypeDef * huart);
bool globalstar_get_fw_version(uint32_t timeout);
#define GLOBALSTAR_START_SYMBOL  0xaa
#endif /* DRIVERS_GLOBALSTAR_H_ */
