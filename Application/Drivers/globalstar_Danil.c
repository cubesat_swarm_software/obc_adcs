/*
 * globalstar_Danil.h
 *
 *  Created on: 5 июн. 2021 г.
 *      Author: Ilia
 */

#ifndef DRIVERS_GLOBALSTAR_DANIL_C_
#define DRIVERS_GLOBALSTAR_DANIL_C_
#include "globalstar_Danil.h"
#include "crc.h"
uint8_t GS_Get_ESN(UART_HandleTypeDef *huart, uint8_t *response)
{
  uint8_t cmd[GS_CMD_NBYTES] = {0xAA, 0x05, 0x01, 0x50, 0xD5};

  HAL_GPIO_WritePin(RTS_GPIO_Port, RTS_Pin, GPIO_PIN_RESET);
  while (HAL_GPIO_ReadPin(CTS_GPIO_Port, CTS_Pin));
  if (HAL_UART_Transmit(huart, cmd, GS_CMD_NBYTES, 100) != HAL_OK)
    return 1;
  HAL_GPIO_WritePin(RTS_GPIO_Port, RTS_Pin, GPIO_PIN_SET);
  while (HAL_GPIO_ReadPin(CTS_GPIO_Port, CTS_Pin) == 0);
  if (HAL_UART_Receive(huart, response, GS_ESN_NBYTES, 100) != HAL_OK)
    return 1;
  return 0;
}

uint8_t GS_Get_Bursts_Remaining(UART_HandleTypeDef *huart, uint8_t *br)
{
  uint8_t cmd[GS_CMD_NBYTES] = {0xAA, 0x05, 0x04, 0xFD, 0x82};
  uint8_t response[GS_QBR_NBYTES];

  HAL_GPIO_WritePin(RTS_GPIO_Port, RTS_Pin, GPIO_PIN_RESET);
  while (HAL_GPIO_ReadPin(CTS_GPIO_Port, CTS_Pin));
  if (HAL_UART_Transmit(huart, cmd, GS_QBR_NBYTES, 100) != HAL_OK)
    return 1;
  HAL_GPIO_WritePin(RTS_GPIO_Port, RTS_Pin, GPIO_PIN_SET);
  while (HAL_GPIO_ReadPin(CTS_GPIO_Port, CTS_Pin) == 0);
  if (HAL_UART_Receive(huart, response, GS_QBR_NBYTES, 100) != HAL_OK)
    return 1;

  if ((response[0] == 0xAA) & (response[2] == 0x04))
    *br = response[3];

  return 0;
}

uint8_t GS_Get_Setup(UART_HandleTypeDef *huart, uint8_t *response)
{
  uint8_t cmd[GS_CMD_NBYTES] = {0xAA, 0x05, 0x07, 0x66, 0xB0};

  HAL_GPIO_WritePin(RTS_GPIO_Port, RTS_Pin, GPIO_PIN_RESET);
  while (HAL_GPIO_ReadPin(CTS_GPIO_Port, CTS_Pin));
  if (HAL_UART_Transmit(huart, cmd, GS_CMD_NBYTES, 100) != HAL_OK)
    return 1;
  HAL_GPIO_WritePin(RTS_GPIO_Port, RTS_Pin, GPIO_PIN_SET);
  while (HAL_GPIO_ReadPin(CTS_GPIO_Port, CTS_Pin) == 0);
  if (HAL_UART_Receive(huart, response, GS_SETUP_NBYTES, 100) != HAL_OK)
    return 1;
  return 0;
}

uint8_t GS_Send_Data(UART_HandleTypeDef *huart, uint8_t NN, uint8_t *data)
{
  uint8_t response[GS_CMD_NBYTES];

  uint8_t cmd[144 + 5];
  cmd[0] = 0xAA;
  cmd[1] = NN + 5;
  cmd[2] = 0x00;
  for (uint8_t i = 0; i < NN; i++)
  {
    *(cmd + 3 + i) = *(data + i);
  }
  uint16_t crc = crc16_lsb(cmd, 3 + NN);
  *(cmd + 3 + NN) = crc;
  *(cmd + 3 + NN + 1) = crc >> 8;

  HAL_GPIO_WritePin(RTS_GPIO_Port, RTS_Pin, GPIO_PIN_RESET);
  while (HAL_GPIO_ReadPin(CTS_GPIO_Port, CTS_Pin));
  if (HAL_UART_Transmit(huart, cmd, NN+5, 100) != HAL_OK)
    return 1;
  HAL_GPIO_WritePin(RTS_GPIO_Port, RTS_Pin, GPIO_PIN_SET);
  while (HAL_GPIO_ReadPin(CTS_GPIO_Port, CTS_Pin) == 0);
  if (HAL_UART_Receive(huart, response, GS_CMD_NBYTES, 100) != HAL_OK)
    return 1;

  if ((response[0] == 0xAA) & (response[2] == 0xFF))
    return 2;

  return 0;
}

uint8_t GS_Send_Setup(UART_HandleTypeDef *huart, uint8_t *data)
{
  uint8_t cmd[144 + 5] = {0xAA, 0x0E, 0x06, 0x00, 0x00, 0x00, 0x00, 0x02, 0x01, 0x18, 0x30, 0x00};
  uint8_t response[GS_CMD_NBYTES];

  volatile uint16_t crc = crc16_lsb(cmd, 12);

  *(cmd + 12) = crc;
  *(cmd + 13) = crc >> 8;


  HAL_GPIO_WritePin(RTS_GPIO_Port, RTS_Pin, GPIO_PIN_RESET);
  while (HAL_GPIO_ReadPin(CTS_GPIO_Port, CTS_Pin));
  if (HAL_UART_Transmit(huart, cmd, 14, 100) != HAL_OK)
    return 1;
  HAL_GPIO_WritePin(RTS_GPIO_Port, RTS_Pin, GPIO_PIN_SET);
  while (HAL_GPIO_ReadPin(CTS_GPIO_Port, CTS_Pin) == 0);

  return 0;
}


#endif /* DRIVERS_GLOBALSTAR_DANIL_C_ */
