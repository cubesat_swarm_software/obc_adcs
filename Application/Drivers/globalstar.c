#include "globalstar.h"
#include "log_utils.h"
#include "stream_buffer.h"
#include "stdio.h"
#include "string.h"
#include "crc.h"
static char * tag = "globalstar";

static struct {
	UART_HandleTypeDef * uart;
	uint8_t				 dma_storage[32];
    uint8_t              BufferStorage[32];
    StaticStreamBuffer_t StreamBuffer_struct;
    StreamBufferHandle_t StreamBuffer;
} Globalstar_state;

static void uart_rx_cb(void);


void globalstar_module_init(UART_HandleTypeDef * huart){
	Globalstar_state.uart = huart;
	HAL_UART_RegisterCallback(Globalstar_state.uart, HAL_UART_RX_COMPLETE_CB_ID, uart_rx_cb);
	Globalstar_state.StreamBuffer = xStreamBufferCreateStatic( sizeof( Globalstar_state.BufferStorage ),
	                                               1,
												   Globalstar_state.BufferStorage,
	                                               &Globalstar_state.StreamBuffer_struct );
	//HAL_GPIO_WritePin(RTS_globalstar_GPIO_Port, RTS_globalstar_Pin, GPIO_PIN_RESET);

}

bool globalstar_get_fw_version(uint32_t timeout){
	uint8_t send_data[5];
	send_data[0] = 0xaa;
	send_data[1] = 0x05;
	send_data[2] = 0x01;//5;
	send_data[3] = 0x50;//74;
	send_data[4] = 0xD5;///93;
/*	uint16_t ctc16 = crc16_lsb(send_data, 2);
	uint8_t ctc16_msb = ctc16&&0x00FF;
	uint8_t ctc16_lsb = (ctc16&&0xFF00)>>8;*/
	HAL_UART_Receive_DMA(Globalstar_state.uart, Globalstar_state.dma_storage, 9);
	HAL_StatusTypeDef res;
	//res = HAL_UART_Receive_IT(Globalstar_state.uart, Globalstar_state.dma_storage, 5);
	//HAL_GPIO_WritePin(RTS_globalstar_GPIO_Port, RTS_globalstar_Pin, GPIO_PIN_SET);
	//osDelay(500);
	//res = HAL_UART_Transmit(Globalstar_state.uart, send_data, 5,2000);
	osDelay(50);
	//HAL_GPIO_WritePin(RTS_globalstar_GPIO_Port, RTS_globalstar_Pin, GPIO_PIN_RESET);
	uint8_t rx_data[5];
	size_t xReceivedBytes = 0;
	uint8_t raw_data[10];
	xReceivedBytes = xStreamBufferReceive(Globalstar_state.StreamBuffer,
											( void * ) raw_data,
											 5,
											 pdMS_TO_TICKS(timeout) );

	return 0;
}


void uart_rx_cb(void){
	int a=0;
	int b = a+1;
	if(Globalstar_state.dma_storage[0] == GLOBALSTAR_START_SYMBOL){
		uint8_t size = Globalstar_state.dma_storage[1];
		BaseType_t xHigherPriorityTaskWoken = pdFALSE;
		size_t xBytesSent = xStreamBufferSendFromISR( Globalstar_state.StreamBuffer,
													 ( void *)&Globalstar_state.dma_storage[2],
													 size-2,
													 &xHigherPriorityTaskWoken);

	}
}


