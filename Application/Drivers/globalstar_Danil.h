#include "main.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#define GS_CMD_NBYTES   5
#define GS_ESN_NBYTES   9
#define GS_QBR_NBYTES   6
#define GS_SETUP_NBYTES 14

uint8_t GS_Get_ESN(UART_HandleTypeDef *huart, uint8_t *esn_data);
uint8_t GS_Query_Bursts_Remaining(UART_HandleTypeDef *huart, uint8_t *qbr);
uint8_t GS_Get_Setup(UART_HandleTypeDef *huart, uint8_t *responce);
uint8_t GS_Send_Data(UART_HandleTypeDef *huart, uint8_t NN, uint8_t *data);
uint8_t GS_Send_Setup(UART_HandleTypeDef *huart, uint8_t *data);
