#ifndef INC_TMP100_H_
#define INC_TMP100_H_

#include <stdint.h>

#define TMP100_COILXY           0b1001000
#define TMP100_COILZ            0b1001010
#define TMP100_IMU              0b1001100
#define TMP100_SW_3V3           0b1001110

#define TMP100_TEMP_REG         0x00
#define TMP100_CONF_REG         0x01
#define TMP100_TLOW_REG         0x02
#define TMP100_THIGH_REG        0x03

//Configuration Register

#define TMP100_CONF_SD          0
#define TMP100_CONF_TM          1
#define TMP100_CONF_POL         2
#define TMP100_CONF_F0          3
#define TMP100_CONF_F1          4
#define TMP100_CONF_R0          5
#define TMP100_CONF_R1          6
#define TMP100_CONF_OS          7

uint8_t TMP100_WriteRegister(uint8_t address, uint8_t reg, uint8_t value);

uint8_t TMP100_ReadRegister(uint8_t address, uint8_t reg, uint16_t *value);

uint8_t TMP100_Init(uint8_t address);

uint8_t TMP100_GetTemperature(uint8_t address, int16_t *value);

#endif /* INC_TMP100_H_ */
