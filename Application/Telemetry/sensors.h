#ifndef FREERTOS_OBC_PEREPHERAL_H
#define FREERTOS_OBC_PEREPHERAL_H

void sensors_init();
void i2c_Scan(I2C_HandleTypeDef *hi2c);
#endif //FREERTOS_OBC_PEREPHERAL_H
