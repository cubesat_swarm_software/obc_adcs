/*
 * log_sd.h
 *
 *  Created on: 30 апр. 2021 г.
 *      Author: Ilia
 */

#ifndef UTILS_LOG_SD_H_
#define UTILS_LOG_SD_H_
void log_sd_init();

int log_sd_str(const char* file_name, char *tag, uint8_t n);
int log_sd_int_array(const char* file_name, const int16_t *mas, const uint8_t n);
int log_sd_float_array(const char* file_name, const float *mas, const uint8_t n);

#endif /* UTILS_LOG_SD_H_ */
