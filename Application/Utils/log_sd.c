/*
 * log_sd.c
 *
 *  Created on: 30 апр. 2021 г.
 *      Author: Ilia
 */
//ATTATION!!! YOU CAN't USE IT IN INTERRUPTS
#include "stm32l4xx_hal.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "log_sd.h"
#include "user_diskio.h"

static SemaphoreHandle_t log_mtx;
static StaticSemaphore_t log_mtx_buffer;

void log_sd_init(){
	log_mtx = xSemaphoreCreateMutexStatic(&log_mtx_buffer);
	xSemaphoreGive(log_mtx);
}

int log_sd_int_array(const char* file_name, const int16_t *mas, const uint8_t n)
{
	xSemaphoreTake(log_mtx, portMAX_DELAY);
	int res = sdcard_write_int_array_to_file(file_name, mas, n);
	xSemaphoreGive(log_mtx);
	return res;
}

int log_sd_str(const char* file_name, char *tag, uint8_t n)
{
	xSemaphoreTake(log_mtx, portMAX_DELAY);
	int res = sdcard_write_str(file_name, tag,n);
	xSemaphoreGive(log_mtx);
	return res;
}

int log_sd_float_array(const char* file_name, const float *mas, const uint8_t n)
{
	xSemaphoreTake(log_mtx, portMAX_DELAY);
	int res = sdcard_write_float_array_to_file(file_name, mas, n);
	xSemaphoreGive(log_mtx);
	return res;
}
