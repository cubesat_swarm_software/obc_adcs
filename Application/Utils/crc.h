/*
 * crc32.h
 *
 *  Created on: 29 ���. 2021 �.
 *      Author: Ilia
 */

#ifndef UTILS_CRC_H_
#define UTILS_CRC_H_
#include "stm32l4xx_hal.h"
#define UpdateCrc32(c) crc32 = crc32_tab[((unsigned char)crc32 ^ (unsigned char)(c)) & 0xff] ^ (crc32 >> 8)

unsigned int crc32_calc(void *pp, unsigned short len);
uint16_t crc16(uint8_t *buf, uint16_t len);
unsigned short crc16_lsb(unsigned char *pData, int length);
#endif /* UTILS_CRC_H_ */
